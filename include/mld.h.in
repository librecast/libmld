/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2023 Brett Sheffield <bacs@librecast.net> */

#ifndef _MLD_H
#define _MLD_H 1

/*
 * MLDv2 library
 *
 * Threads:
 * - NETLINK - detects interface changes and reports them to STATE
 * - LISTEN  - listens for MLD traffic and reports events to STATE
 * - TIMER   - creates jobs for STATE to expire MLD records
 * - STATE   - handles all reads and writes to MLD cache, notifies WATCH of events
 * - WATCH   - receives events from STATE and creates callback threads for watchers
 */

#include <limits.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/icmp6.h>
#include <pthread.h>
#include <stdint.h>

@MLD_BLOOM_FILTER@
#ifdef MLD_BLOOM_FILTER
# include <immintrin.h>
# define BLOOM_SZ 16777216
# define BLOOM_VECTORS BLOOM_SZ / VECTOR_BITS
# define BLOOM_HASHES 8 /* optimal = LOG2(BLOOM_SZ / ENTRIES) */
#endif
#define MLD_TIMEOUT 125 /* seconds before MLD record expires */
#define MLD_TIMER_INTERVAL 1 /* length of timer tick in seconds */
#define IPV6_BYTES 16

/* See RFC 3810 */

/* MALI = Multicast Address Listening Interval */
/* LLQT = Last Listener Query Time */

#define MLD2_ROBUSTNESS 2               /* 9.14.1.  Robustness Variable */
_Static_assert(MLD2_ROBUSTNESS <= 0x7, "QRV > 3 bits");
#define MLD2_ALL_NODES "ff02::1"        /* all MLDv2-capable routers */
#define MLD2_CAPABLE_ROUTERS "ff02::16" /* all MLDv2-capable routers */
#define MLD2_LISTENER_REPORT 143        /* Multicast Listener Report messages */
#define MLD2_QI MLD_TIMEOUT             /* Query Interval */
#define MLD2_QRI 10000                  /* Query Response Interval (RFC suggests 10000 = 10s) */

/* RFC says MALI MUST be Robustness x QI + QRI */
#define MLD2_MALI MLD2_ROBUSTNESS * MLD2_QI + MLD2_QRI

/* 9.5.  Other Querier Present Timeout
 * ([Robustness Variable] times ([Query Interval]) plus (one
   half of [Query Response Interval]).*/
#define MLD2_OTHER_QUERIER_TIMEOUT MLD2_ROBUSTNESS * MLD2_QI + MLD2_QRI / 2000

/* Current State Record */
#define MODE_IS_INCLUDE 1
#define MODE_IS_EXCLUDE 2

/* Filter Mode Change Record */
#define CHANGE_TO_INCLUDE_MODE 3
#define CHANGE_TO_EXCLUDE_MODE 4

/* Source List Change Record */
#define ALLOW_NEW_SOURCES 5
#define BLOCK_OLD_SOURCES 6

/* 9.14.1.  Robustness Variable */
#define MLD2_ROBUSTNESS 2

/* flags */
enum {
	MLD_DONTWAIT = 1
};

/* Event Types */
typedef enum {
	MLD_EVENT_JOIN = 1,
	MLD_EVENT_PART = 2,
	MLD_EVENT_IFUP = 4,
	MLD_EVENT_IFDOWN = 8,
	MLD_EVENT_MAX
} mld_event_type_t;
#define MLD_EVENT_ALL ((MLD_EVENT_MAX - 1) << 1) - 1

/* port (or service) to use for MLD event notifications */
#define MLD_EVENT_SERV 4242

#define aitoin6(ai) &(((struct sockaddr_in6 *)ai->ai_addr)->sin6_addr)

typedef struct mld_s mld_t;
typedef struct mld_addr_rec_s mld_addr_rec_t;
typedef struct mld_grp_list_s mld_grp_list_t;
typedef struct mld_query_msg_s mld_query_msg_t;
typedef struct mld_grp_s mld_grp_t;
typedef struct mld_msg_s mld_msg_t;
typedef struct mld_iface_s mld_iface_t;
typedef struct mld_txn_s mld_txn_t;
typedef struct mld_watch_s mld_watch_t;

#ifdef MLD_BLOOM_FILTER
typedef struct mld_filter_s mld_filter_t;
typedef struct mld_timerjob_s mld_timerjob_t;
#endif

struct mld_grp_s {
	mld_t *mld;
	unsigned int iface;
	struct in6_addr *grp;
};

/* set loglevel */
void mld_loglevel_set(int level);

/* initialize / free state machine */
mld_t *mld_init(int flags);

/* free MLD objects */
void mld_free(mld_t *mld);

/* start MLD snooping */
mld_t *mld_start(mld_t *mld);

/* stop MLD snooping */
void mld_stop(mld_t *mld);

/* return true (1) if filter contains addr, false (0) if not, -1 on error */
int mld_filter_grp_cmp(mld_t *mld, unsigned int ifx, struct in6_addr *addr);

/* add group address to interface filter
 * return result of mld_filter_grp_cmp() before adding. -1 on error */
int mld_filter_grp_add(mld_t *mld, unsigned int ifx, struct in6_addr *addr);

/* delete group address from interface filter */
int mld_filter_grp_del(mld_t *mld, unsigned int ifx, struct in6_addr *addr);

/* get timer for group on interface */
int mld_filter_timer_get(mld_t *mld, unsigned int ifx, struct in6_addr *addr, struct timespec *ts);

/* set timer for group on interface */
int mld_filter_timer_set(mld_t *mld, unsigned int ifx, struct in6_addr *addr, struct timespec *ts);

/* set timer for group on interface to now + s seconds */
int mld_filter_timer_set_s(mld_t *mld, unsigned int ifx, struct in6_addr *addr, int s);

/* add watch */
mld_watch_t *mld_watch_add(mld_t *mld, unsigned int ifx, struct in6_addr *addr,
		void (*f)(mld_watch_t *), void *arg, int flags);

/* delete watch */
void mld_watch_del(mld_watch_t *watch);

mld_t *mld_watch_mld(mld_watch_t *watch);
unsigned int mld_watch_ifx(mld_watch_t *watch);
struct in6_addr *mld_watch_grp(mld_watch_t *watch);
void *mld_watch_arg(mld_watch_t *watch);
pthread_t mld_watch_tid(mld_watch_t *watch);
int mld_watch_flags(mld_watch_t *watch);

/* block until notification received for addr on interface index ifx. If ifx is
 * zero, all interfaces are watched.  Returns 0 on success, or -1 on error and
 * errno is set to indicate the error.
 *
 * The flags argument is formed by ORing one or more of the following values:
 *
 *  MLD_DONTWAIT
 *      Enables  nonblocking  operation; if the operation would block, the call
 *      fails with the error EWOULDBLOCK.*/
int mld_wait(mld_t *mld, unsigned int ifx, struct in6_addr *addr, int flags);

#endif /* _MLD_H */
