/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "testnet.h"
#include <mld_pvt.h>

int main(void)
{
	mld_t *mld;

	test_cap_require(CAP_NET_RAW);
	test_name("mld_start() / mld_stop()");
	mld = mld_start(NULL);
	test_assert(mld != NULL, "mld_t allocated");
	if (!mld) return test_status;

	/* ensure threads started */
	for (int i = 0; i < MLD_THREADS; i++) {
		test_assert(mld->q[i] != NULL, "thread %i started", i);
	}

	test_assert(mld != NULL, "mld_t allocated");
	mld_stop(mld);
	mld_free(mld);
	return test_status;
}
